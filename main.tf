resource "google_compute_network" "this" {
  name = "terraform-network"
  auto_create_subnetworks = false
  description = "terraform-network"
}

resource "google_compute_subnetwork" "taiwan" {
    name = "terraform-subnetwork-taiwan"
    ip_cidr_range = "10.140.0.0/20"
    network = google_compute_network.this.self_link
    region = var.region-taiwan
    private_ip_google_access = true
}
/*
resource "google_compute_subnetwork" "tokyo" {
    name = "terraform-subnetwork-tokyo"
    ip_cidr_range = "10.142.0.0/20"
    network = google_compute_network.this.self_link
    region = var.region-tokyo
    private_ip_google_access = true
}

resource "google_container_cluster" "this" {
  name = "terraform-container-cluster"
  location = var.region-taiwan
  remove_default_node_pool = true
  initial_node_count = 1
  network = google_compute_network.this.name
  subnetwork = google_compute_subnetwork.taiwan.name
}
*/
resource "google_compute_instance" "testnetworkreliability" {
  boot_disk {
    initialize_params {
      image = "ubuntu-2204-lts"
    }
  }
  machine_type = "e2-micro"
  name = "testnetworkreliability"
  zone = var.zone-taiwan
  network_interface {
    subnetwork = google_compute_subnetwork.taiwan.name
    access_config {
      
    }
  }
  
  metadata_startup_script = "sudo apt-get install -y nginx-core"
}

resource "google_compute_instance_group" "this" {
  name        = "terraform-compute-instance-group"
  network = google_compute_network.this.id
  zone = var.zone-taiwan
  instances = [ google_compute_instance.testnetworkreliability.id ]
  named_port {
    name = "http"
    port = 80
  }
}

resource "google_compute_firewall" "this" {
  name    = "terraform-compute-firewall"
  network = google_compute_network.this.name
  source_ranges = [ local.my_ip_addr ]

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80", "443"]
  }

  priority = 10000
}

resource "google_compute_firewall" "cloud-iap" {
  name = "cloud-iap"
  network = google_compute_network.this.name
  source_ranges = [ "35.235.240.0/20" ]
  allow {
    protocol = "tcp"
    ports = [ "22", "3389" ]
  }
  priority = 999
}


resource "google_compute_firewall" "health-check-service" {
  name = "health-check-service"
  network = google_compute_network.this.name
  source_ranges = [ "35.191.0.0/16","130.211.0.0/22","209.85.152.0/22","209.85.204.0/22" ]
  allow {
    protocol = "tcp"
    ports = [ "80", "443","8080" ]
  }
  priority = 999
}

resource "google_compute_global_address" "this" {
  name = "terraform-compute-global-address"
}

resource "google_compute_http_health_check" "this" {
  name               = "terraform-health-check"
  request_path       = "/"
  check_interval_sec = 1
  timeout_sec        = 1
}

resource "google_compute_backend_service" "this" {
  name = "terraform-compute-backend-service"
  health_checks = [ google_compute_http_health_check.this.id ]
  backend {
    group = google_compute_instance_group.this.id
  }
}

resource "google_compute_url_map" "this" {
  name            = "terraform-compute-url-map"
  default_service = google_compute_backend_service.this.id
}


resource "google_compute_target_http_proxy" "this" {
  name     = "terraform-target-http-proxy"
  url_map  = google_compute_url_map.this.id
}

resource "google_compute_global_forwarding_rule" "this" {
  name = "terraform-compute-global-forwarding-rule"
  provider = google-beta
  ip_protocol = "TCP"
  load_balancing_scheme = "EXTERNAL"
  port_range = "80"
  target = google_compute_target_http_proxy.this.id
  ip_address = google_compute_global_address.this.id
}

output "loadbalance-ip" {
  value = google_compute_global_address.this.address
}
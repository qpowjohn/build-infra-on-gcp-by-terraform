variable "region-taiwan" {
  default = "asia-east1"
}

variable "region-tokyo" {
  default = "asia-northeast1"
}

variable "zone-taiwan" {
  default = "asia-east1-a"
}

# ----get my ip ----
data "http" "my_public_ip" {
  url = "https://ifconfig.co/json"
  request_headers = {
    Accept = "application/json"
  }
}

locals {
  my_ip_addr = jsondecode(data.http.my_public_ip.response_body).ip
}
# ----get my ip ----
# ----get project name ----

locals {
  project = split(" ",split("\n",file("/home/jack/.config/gcloud/configurations/config_default"))[2])[2]
}
# ----get project name ----